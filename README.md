# Detect Vehicle Damage with Deep Learning (in beta)

![](./resources/auto_detect.png)
###### *'Ravin.ai (courtesy)'*

## About
- Major advancements in deep learning have helped accelerate the accomplishments of business problems that used to be deemed essentially impossible. Whether that's facial/image recognition, autonomous driving, text analytics, etc.
- One specific area deep learning is making advancements is in the area of computer vision. And to further specify in regards to this project, is computer vision that can detect irregular patterns or "damage" on the surface of automobiles.

## Business Problem
- Sectors like automobile insurance, used car industry, car auctions, and others are required estimate the price of a car during a certain evaluation period. For the most part, this process currently depends on an indivudal (or estimator) to manually determine the price of the vehicle from viewing a variety of images.

## Approach to Solution
- This approach will consist of gathering relevant data, feature engineering, and comparing/evaluating a variety of deep learning algorithms (CNN, R-CNN, Mask R-CNN) in detecting vehicle damage.